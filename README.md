# Интерфейс **_Collection_** <Собинников Павел 603Б1>

### **Интерфейс:** _Collection_  
##### **Extends:** _-_
##### **Методы:**
    boolean add(T e);
    void clear();
    boolean isEmpty();
    int size();

### **Интерфейс:** _List<T>_  
##### **Extends:** _Collection<T>_  
##### **Методы:**
    void add(int index, T element);
    T get(int index);
    T remove(int index);

### **Класс:** _ArrayList<T>_  
##### **Implements:** _List<T>_  
##### **Поля и Методы:**
    private int _arr_size;
    private int _size;
    private Object[] eData;
    ArrayList();
    public void add(int index, T element);
    public T get(int index);
    public T remove(int index);
    public boolean add(T e);
    public void clear();
    public boolean isEmpty();
    public int size();
    private T eData(int index);
    void print();

### **Класс:** _LinkedList<T>_  
##### **Implements:** _List<T>_  
##### **Вложенные классы:** _Node<T>_ 
    T item;
    Node<T> next;
    Node<T> prev;
    Node(Node<T> prev, T element, Node<T> next);
##### **Поля и Методы:**
    private int _size = 0;
    private Node<T> _head;
    private Node<T> _tail;
    public boolean add(T e);
    public void clear();
    public boolean isEmpty();
    public int size();
    public void add(int index, T element);
    public T get(int index);
    public T remove(int index);
    private Node<T> Node(int index);
    private void push_back(T e);
    void print();