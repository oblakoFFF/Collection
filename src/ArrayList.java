public class ArrayList<T> implements List<T> {
    private int _arr_size = 5;
    private int _size = 0;
    private Object[] eData;

    ArrayList() {
        eData = new Object[_arr_size];
    }

    public void add(int index, T element) {
        System.arraycopy(eData, index, eData, index + 1, _size - index);
        eData[index] = element;
        ++_size;
    }

    public T get(int index) {
        return eData(index);
    }

    public T remove(int index) {
        T old = eData(index);
        if (_size - index - 1 > 0) {
            System.arraycopy(eData, index + 1, eData, index, _size - index - 1);
        }

        eData[--_size] = null;
        return old;
    }

    public boolean add(T e) {
        eData[_size++] = e;
        return true;
    }

    public void clear() {
        for(int i = 0; i < _size; ++i) {
            eData[i] = null;
        }

        _size = 0;
    }

    public boolean isEmpty() {
        return _size == 0;
    }

    public int size() {
        return _size;
    }

    @SuppressWarnings("unchecked")
    private T eData(int index) { return (T) eData[index]; }

    void print() {
        for(int i = 0; i < _arr_size; ++i) {
            System.out.println(eData[i]);
        }

    }
}