public interface Collection<T> {
    boolean add(T e);
    void clear();
    boolean isEmpty();
    int size();
}