public class LinkedList<T> implements List<T> {
    private static class Node<T>{
        T item;
        Node<T> next;
        Node<T> prev;

        Node(Node<T> prev, T element, Node<T> next){
            this.prev = prev;
            this.item = element;
            this.next = next;
        }
    }

    private int _size = 0;
    private Node<T> _head;
    private Node<T> _tail;

    LinkedList(){}

    public boolean add(T e) {
        push_back(e);
        return true;
    }

    public void clear() {
        for(Node<T> i_ptr = _head; i_ptr != null; ){
            Node<T> next = i_ptr.next;
            i_ptr.prev = null;
            i_ptr.item = null;
            i_ptr.next = null;
            i_ptr = next;
        }
        _head = _tail = null;
        _size = 0;
    }

    public boolean isEmpty() {
        return _size == 0;
    }

    public int size() {
        return _size;
    }

    public void add(int index, T element) {
        if (index == _size) push_back(element);
        else {
            Node<T> succ = Node(index);
            Node<T> newNode = new Node<>(succ.prev, element, succ);
            succ.prev.next = newNode;
            succ.prev = newNode;
            _size++;
        }
    }

    public T get(int index) {
        return Node(index).item;
    }

    public T remove(int index) {
        if (isEmpty()) return null;

        Node<T> succ = Node(index);
        T element = succ.item;
        if (succ.prev == null) _head = succ.next;
        else {
            succ.prev.next = succ.next;
            succ.prev = null;
        }
        if (succ.next == null) _tail = succ.prev;
        else {
            succ.next.prev = succ.prev;
            succ.next = null;
        }
        succ.item = null;
        _size--;
        return element;
    }

    private Node<T> Node(int index){
        if (index < (_size << 1)){
            Node<T> i_ptr = _head;
            for(int i = 0; i < index; ++i)
                i_ptr = i_ptr.next;
            return i_ptr;
        }
        else{
            Node<T> i_ptr = _tail;
            for(int i = _size - 1; i > index; ++i)
                i_ptr = i_ptr.prev;
            return i_ptr;
        }
    }

    private void push_back(T e){
        Node<T> newNode = new Node<>(_tail, e, null);
        if (_tail == null) _head = newNode;
        else _tail.next = newNode;
        _tail = newNode;
        _size++;
    }

    void print(){
        for(Node<T> i_ptr = _head; i_ptr != null; ){
            System.out.println(i_ptr.item);
            i_ptr = i_ptr.next;
        }
    }
}
