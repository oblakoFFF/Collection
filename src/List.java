public interface List<T> extends Collection<T> {
    void add(int index, T element);
    T get(int index);
    T remove(int index);
}
